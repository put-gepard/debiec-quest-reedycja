#include <algorithm>
#include <mpi.h>
#include <stdint.h>
#include <string>
#include <unistd.h>
#include <vector>
#include <random>

#define debug(format, ...) \
    fprintf(stderr, "%4d P%d " format, my_clock, my_rank, ##__VA_ARGS__)

#define NOREQ 9999

using namespace std;

const int group_size = 3;
const int guide_count = 2;
const int delay = 1;

auto rng = default_random_engine {};

enum Tag { REQUEST,
    ACK,
    START,
    RELEASE };
struct ElemQ {
    int pid;
    int reqTime;
};
struct Packet {
    Tag tag;
    int time;
    int reqTime;
    int myGroup[group_size] = { -1 };
};

time_t s;

vector<int> group;

MPI_Request request = { 0 };
MPI_Status status = { 0 };

Packet pkg;

int max_possible_rank = 8;
int startTime = -1;

vector<ElemQ> after;

bool inJured = false;
bool asking_for_crit = false;
bool doing_stuff_at_dembiec_pkm = false;
bool chef = false;
bool awaiting_recv = false;

int my_rank;
int my_clock = 0;
int ack_count = 0;

string queue_string(vector<ElemQ> q)
{
    int N = q.size();
    string res = "";
    for (int i = 0; i < N; i++) {
        res += to_string(q[i].pid);
        if (i < N - 1)
            res += ", ";
    }
    return "[" + res + "]";
}

string queue_string_int(vector<int> q)
{
    int N = q.size();
    string res = "";
    for (int i = 0; i < N; i++) {
        res += to_string(q[i]);
        if (i < N - 1)
            res += ", ";
    }
    return "[" + res + "]";
}

string dog(int q[group_size])
{
    int N = group_size;
    string res = "";
    for (int i = 0; i < N; i++) {
        res += to_string(q[i]);
        if (i < N - 1)
            res += ", ";
    }
    return "[" + res + "]";
}

void send_message(int destination, Tag tag, int reqTime = NOREQ)
{
    Packet pkg { .tag = tag, .time = my_clock, .reqTime = reqTime };
    if (pkg.tag == START || pkg.tag == RELEASE) {
        for (int i = 0; i < group_size; i++) {
            pkg.myGroup[i] = group[i];
        }
    }
    MPI_Send(&pkg, sizeof(Packet), MPI_BYTE, destination, 0, MPI_COMM_WORLD);
}

bool recv_message(int& src, Packet* pkg)
{
    if (!awaiting_recv) {
        MPI_Irecv(pkg, sizeof(Packet), MPI_BYTE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &request);
        awaiting_recv = true;
    }

    int completed;
    MPI_Test(&request, &completed, &status);

    if (!completed) {
        return false;
    }

    src = status.MPI_SOURCE;
    awaiting_recv = false;
    return true;
}

void update_lamport(int update_clock = 0)
{
    my_clock = max(my_clock, update_clock) + 1;
}

bool sortFunc(ElemQ a, ElemQ b)
{
    if (a.reqTime == b.reqTime) {
        return a.pid < b.pid;
    }
    return a.reqTime < b.reqTime;
}

void RESP(int t)
{
    int src;
    if (!recv_message(src, &pkg))
        return;

    update_lamport(pkg.time);

    if (pkg.tag == REQUEST && !doing_stuff_at_dembiec_pkm) {
        bool eval;
        if (pkg.time == t) {
            eval = src < my_rank;
        } else {
            eval = pkg.time < t;
        }

        ElemQ e = { .pid = src, .reqTime = pkg.reqTime };
        after.push_back(e);
        sort(after.begin(), after.end(), sortFunc);
        if ((eval && asking_for_crit && !doing_stuff_at_dembiec_pkm) || (!doing_stuff_at_dembiec_pkm && !asking_for_crit)) {
            update_lamport();
            send_message(src, ACK);
        }
    } else if (pkg.tag == 1 && !doing_stuff_at_dembiec_pkm) {
        if (pkg.reqTime > startTime)
            ack_count++;
    } else if (pkg.tag == RELEASE) {
        debug("wywalam z kolejki ziomow o numerze %s, pzdr\n", dog(pkg.myGroup).c_str());
        debug("ale to będzie ochydne %s\n", queue_string(after).c_str());
        for (int i = 0; i < group_size; i++) {
            int idx = pkg.myGroup[i];
            auto it = find_if(after.begin(), after.end(), [idx](const ElemQ& obj) {
                return obj.pid == idx;
            });
            if (it != after.end()) {
                after.erase(it);
            }
            debug("wywaliłem w końcu, zostali: %s\n", queue_string(after).c_str());

            auto it2 = find(group.begin(), group.end(), src);
            if (doing_stuff_at_dembiec_pkm && it2 != group.end()) {
                debug("hello\n");
                doing_stuff_at_dembiec_pkm = false;
            }
        }
    }
}

void WANT()
{
    update_lamport();
    debug("hihi!\n");

    int t = my_clock;
    ack_count = 0;
    for (int rank = 0; rank < max_possible_rank; rank++) {
        if (rank != my_rank) {
            send_message(rank, REQUEST, t);
        }
    }

    asking_for_crit = true;
    while (ack_count < max_possible_rank - group_size * guide_count && after.size() < group_size - 1) {
        RESP(t);
        sleep(delay);
    }
    ElemQ e = { .pid = my_rank, .reqTime = t };
    after.push_back(e);
    sort(after.begin(), after.end(), sortFunc);

    asking_for_crit = false;
}

void FREE()
{
    update_lamport();
    for (ElemQ e : after)
        send_message(e.pid, ACK);
    // after.clear();
}

void GROUP()
{
    int pos = -1;
    for (int i = 0; i < after.size(); i++) {
        if (after[i].pid == my_rank) {
            pos = i;
        }
    }

    if (pos == -1) {
        debug("error, you stupid!\n");
        return;
    }

    if ((pos + 1) % group_size == 0) {
        debug("widac ze zawsze mialem zadatki na szefa\n");
        for (int i = pos - 1; i >= 0 && i > pos - group_size; i--) {
            group.push_back(after[i].pid);
        }
        group.push_back(after[pos].pid);

        debug("jestem szefem najlepszym, moje ziomy to %s\n", queue_string_int(group).c_str());

        for (auto u : group) {
            send_message(u, START);
        }
        chef = true;
        doing_stuff_at_dembiec_pkm = true;
    }

    if (time(0) - s > 12) {
        debug("debile co w robicie!\n");
        shuffle(begin(after), end(after), rng); 
    }

    int src;
    if (!recv_message(src, &pkg))
        return;

    if (pkg.tag == START) {
        startTime = my_clock;
        debug("zostałem wystartowany, moja grupa to %s\n", dog(pkg.myGroup).c_str());
        for (int i = 0; i < group_size; i++) {
            group.push_back(pkg.myGroup[i]);
        }

        doing_stuff_at_dembiec_pkm = true;
    } else if (pkg.tag == RELEASE) {
        debug("wywalam z kolejki ziomow o numerze %s, pzdr\n", dog(pkg.myGroup).c_str());
        debug("ale to będzie ochydne %s\n", queue_string(after).c_str());
        for (int i = 0; i < group_size; i++) {
            int idx = pkg.myGroup[i];
            auto it = find_if(after.begin(), after.end(), [idx](const ElemQ& obj) {
                return obj.pid == idx && idx != my_rank && pkg.time > obj.reqTime;
            });
            if (it != after.end()) {
                after.erase(it);
            }
            debug("wywaliłem w końcu, zostali: %s\n", queue_string(after).c_str());
        } 
    }
}

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &max_possible_rank);

    debug("my_rank %d (max_rank %d)\n", my_rank, max_possible_rank);

    while (true) {
        debug("chce na dembiec ale na razie jestem wolny jak ptak\n");
        WANT();
        debug("jestem na tajnym spotkaniu poszukiwania grupy, ");
        debug("moja kolejka to %s\n", queue_string(after).c_str());
        s = time(0);
        while (!doing_stuff_at_dembiec_pkm) {
            GROUP();
            sleep(delay);
        }

        if (chef) {
            debug("jestem szefem wszechświata!\n");
        }

        debug("haha jestem na wycieczce\n");
        time_t start = time(0);
        while (time(0) - start < 5)
            ;

        if (chef) {
            debug("wysyłam release\n");
            for (int i = 0; i < max_possible_rank; i++) {
                send_message(i, RELEASE);
            }
            chef = false;
        }

        while (doing_stuff_at_dembiec_pkm) {
            RESP(NOREQ);
        }
        group.clear();
        debug("powrót\n");

        FREE();

        int random = rand() % 100;
        if (random > 50) {
            inJured = true;
            debug("aaaaa straciłem wszystkie zęby i palec!\n");
        }
        start = time(0);
        while(inJured && time(0) - start < 5) {
            RESP(NOREQ);
        }
        inJured = false;
    }

    MPI_Finalize();
    return 0;
}
