all: build

build:
	mpic++ -O2 --std=c++17 main.cpp -o app

run: app
	mpirun -oversubscribe -np 8 ./app
